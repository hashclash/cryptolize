import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import {BehaviorSubject} from "rxjs/Rx";
import {Subject} from "rxjs/Subject";
import {Koinex} from '../models/Koinex'
import { Response } from '@angular/http/src/static_response';

@Injectable()
export class DataProvider {
    koinexApi:string;
    zeppayApi:string;
    unoApi: string;
    coinSecureApi:string
    buyUCoinBTC:string;
 
    constructor(private http:Http){
        this.koinexApi ="https://koinex.in/api/ticker";
        this.zeppayApi= "https://live.zebapi.com/api/v1/ticker?currencyCode=INR";
        this.unoApi = "https://www.unocoin.com/trade?";
        this.coinSecureApi = "https://api.coinsecure.in/v1/exchange/ticker";
        this.buyUCoinBTC="https://www.buyucoin.com/api/v1/btc/";
    }
 
    getKoinex(): Observable<any>{
       return this.http.get(this.koinexApi).map(this.extractData).catch(this.handleError);
    }

    getZeppay():Observable<any>{
        return this.http.get(this.zeppayApi).map(this.extractData).catch(this.handleError);

    }

    getUno(type):Observable<any>{
        return this.http.get(this.unoApi+type).map(this.extractData).catch(this.handleError);
    }

    getCoinSecure():Observable<any>{
        return this.http.get(this.coinSecureApi).map(this.extractData).catch(this.handleError);
    }
    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    }
    private handleError(error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    getBuyUCoinBTC(){
        return this.http.get(this.buyUCoinBTC).map(this.extractData).catch(this.handleError);

    }
}