import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MaterialModule} from './material.module'
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { Http } from '@angular/http';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import { HttpModule } from '@angular/http';



import { AppComponent } from './app.component';
import {BitcoinPage} from './pages/bitcoin/bitcoin'
import {BitcoinCashPage} from './pages/bitcoincash/bitcoincash'
import {LitecoinPage} from './pages/Litecoin/litecoin'
import {EtherPage} from './pages/ether/ether'
import {RipplePage} from './pages/ripple/ripple'
import {DataProvider} from './providers/dataProvider'

@NgModule({
  declarations: [
    AppComponent,
    BitcoinPage,
    BitcoinCashPage,
    RipplePage,
    EtherPage,
    LitecoinPage
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    BrowserAnimationsModule,
    HttpClientModule,
    HttpModule
  ],
  providers: [DataProvider],
  bootstrap: [AppComponent]
})
export class AppModule { }
