import {Component} from '@angular/core'
import { Koinex } from '../../models/Koinex';
import { Zeppay } from '../../models/Zeppay';
import { DataProvider } from '../../providers/dataProvider';


@Component({
    selector:'litecoin',
    templateUrl:'litecoin.html'
})


export class LitecoinPage{
    public koinex: Koinex;
    public zeppay: Zeppay;
  


    constructor(private dataProvider: DataProvider) {
        this.koinex = new Koinex();
        this.zeppay = new Zeppay();
    }

    ngOnInit() {
        this.dataProvider.getKoinex()
            .subscribe(res => {
                console.dir(res);
                this.koinex = res;
            });

        this.dataProvider.getZeppay()
            .subscribe(res => {
                console.dir(res);
                this.zeppay = res;
            });
       
    }
}