import { Component, OnInit } from '@angular/core'
import { DataProvider } from '../../providers/dataProvider';
import { log } from 'util';
import { Koinex } from '../../models/Koinex';
import { Zeppay } from '../../models/Zeppay';
import { CoinSecure } from '../../models/CoinSecure';
import { Console } from '@angular/core/src/console';
import {BuyUCoinBTC} from '../../models/BuyUCoinBTC'


@Component({
    selector: 'bitcoin',
    templateUrl: 'bitcoin.html',
    styleUrls: ['bitcoin.scss']
})


export class BitcoinPage implements OnInit {
    public koinex: Koinex;
    public zeppay: Zeppay;
  


    constructor(private dataProvider: DataProvider) {
        this.koinex = new Koinex();
        this.zeppay = new Zeppay();
       

    }

    ngOnInit() {
        this.dataProvider.getKoinex()
            .subscribe(res => {
                console.dir(res);
                this.koinex = res;
            });

        this.dataProvider.getZeppay()
            .subscribe(res => {
                console.dir(res);
                this.zeppay = res;
            });
       
    }



}