export class Zeppay{
    market:number;
    buy:number;
    sell:number;
    currency:string;
    volume:number;
    virtualCurrency:string;
}