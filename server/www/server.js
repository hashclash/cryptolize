const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors')

app.use(bodyParser.json());

var corsOptions = {
  origin: 'https://127.0.0.1:8000',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204 
}

app.use(cors(corsOptions))

//end-point to send all the cats

app.route('/api/cats').get((req, res) => {
  res.send({
    cats: [{
      'name': 'lucy'
    }, {
      'name': 'lily'
    }]
  });
});
//end-point to send specific cat based upon the cat name in the url
//here we are going to use the route-param because, cat name can be dynamic

app.route('/api/cats/:name').get((req, res) => {
  const requestedCatName = req.params['name']

  res.send({
    name: requestedCatName
  })
});

//end-point to post data 
app.route('/api/cats').post((req, res) => {
  res.send(201, req.body);
});

//endpoint to update the data
app.route('/api/cats/:name').put((req, res) => {
  res.send(200, req.body);
});

//end-point to delete
//204 req successful, but no data to send back
app.route('/api/cats/:name').delete((req, res) => {
  res.sendStatus(204);
});

app.listen(8000, () => {
  console.log('Server started!');
});
